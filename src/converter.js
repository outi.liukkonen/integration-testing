const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    } ,
    hexToRGB: (hex) => {
        const color = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        const redRGB = parseInt(color[1], 16);
        const greenRGB = parseInt(color[2], 16);
        const blueRGB = parseInt(color[3], 16);
        const redBasic = redRGB.toString(10);
        const greenBasic = greenRGB.toString(10);
        const blueBasic = blueRGB.toString(10);

        return redBasic + ", " + greenBasic + ", " + blueBasic;
    }
}